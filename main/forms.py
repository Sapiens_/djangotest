from .models import Author, Genre, Book
from django.forms import ModelForm, TextInput, DateInput, ModelChoiceField


class AuthorForm(ModelForm):
    class Meta:
        model = Author
        fields = ['name', 'info']
        widgets = {'name': TextInput(attrs={'class': 'input', 'placeholder': 'Введите имя'}), 'info': TextInput(attrs={'class': 'input', 'placeholder': 'Добавьте примечание'})}


class GenreForm(ModelForm):
    class Meta:
        model = Genre
        fields = ['name', 'info']
        widgets = {'name': TextInput(attrs={'class': 'input', 'placeholder': 'Введите жанр'}), 'info': TextInput(attrs={'class': 'input', 'placeholder': 'Добавьте примечание'})}


class BookForm(ModelForm):
    class Meta:
        model = Book
        author = ModelChoiceField(queryset=Author.objects.all(), empty_label='Выберите автора', to_field_name='автор')
        genre = ModelChoiceField(queryset=Genre.objects.all(), empty_label='Выберите жанр', to_field_name='жанр')
        fields = ['name', 'count_page', 'date', 'author', 'genre']
        widgets = {'name': TextInput(attrs={'class': 'input', 'placeholder': 'Введите название'}),
                   'count_page': TextInput(attrs={'class': 'input', 'placeholder': 'Введите количество страниц'}),
                   'date': DateInput(attrs={'class': 'input', 'placeholder': 'Добавьте дату', 'type': 'date'}),
                   }
