from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.shortcuts import render
from .forms import AuthorForm, GenreForm, BookForm
from .models import Author, Genre, Book


def index(request):
    return render(request, 'main/index.html')


def index_author(request):
    if request.method == 'POST':
        form = AuthorForm(request.POST)
        if form.is_valid():
            pre_save = form.save(commit=False)
            name = form.cleaned_data.get('name')
            flag = 0
            bd_author = Author.objects.all()
            for author in bd_author:
                if author.name == name:
                    flag = 1
            if flag == 0:
                pre_save.save()
        error = 'Неправильно введены данные'
    form = AuthorForm()
    error = ''
    context = {
        'form': form,
        'error': error
    }

    return render(request, 'main/author.html', context)


def index_genre(request):
    if request.method == 'POST':
        form = GenreForm(request.POST)
        if form.is_valid():
            pre_save = form.save(commit=False)
            name = form.cleaned_data.get('name')
            flag = 0
            bd_genre = Genre.objects.all()
            for genre in bd_genre:
                if genre.name == name:
                    flag = 1
            if flag == 0:
                pre_save.save()
        error = 'Неправильно введены данные'
    form = GenreForm()
    error = ''
    context = {
        'form': form,
        'error': error
    }
    return render(request, 'main/genre.html', context)


def index_book(request):
    if request.method == 'POST':
        form = BookForm(request.POST)
        if form.is_valid():
            form.save()
        error = 'Неправильно введены данные'
    form = BookForm()
    error = ''
    context = {
        'form': form,
        'error': error
    }
    return render(request, 'main/book.html', context)


def index_update(request, id):
    try:
        all_author = Author.objects.all()
        all_genre = Genre.objects.all()
        book = Book.objects.get(id=id)
        book.date = book.date.strftime('%Y-%m-%d')
        if request.method == "POST":
            book.name = request.POST.get("name")
            book.count_page = request.POST.get("count_page")
            book.date = request.POST.get("date")
            book.maker = Author.objects.get(id=request.POST.get("author"))
            book.drinks = Genre.objects.get(id=request.POST.get("genre"))
            book.save()
            return HttpResponseRedirect("/post_book")
        else:
            return render(request, 'main/update.html', {'book': book, 'all_author': all_author, 'all_genre': all_genre})
    except Book.DoesNotExist:
        return HttpResponseNotFound("<h2>Не найдена запись</h2>")


def index_post_book(request):
    book = Book.objects.all()
    return render(request, 'main/post_book.html', {'book': book})
