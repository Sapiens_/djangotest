from django.db import models


class Author(models.Model):
    name = models.TextField('Имя')
    info = models.TextField('Примечание')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'


class Genre(models.Model):
    name = models.TextField('Жанр')
    info = models.TextField('Примечание')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'


class Book(models.Model):
    name = models.TextField('Название')
    count_page = models.TextField('Количество страниц')
    date = models.DateField('Дата публикации')
    author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True)
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = 'Книги'