from django.urls import path
from . import views


urlpatterns = [
    path('', views.index),
    path('author', views.index_author),
    path('genre', views.index_genre),
    path('book', views.index_book),
    path('post_book', views.index_post_book),
    path('update/<int:id>', views.index_update)
]
